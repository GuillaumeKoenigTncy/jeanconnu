# pylint: disable=missing-function-docstring, import-error, line-too-long

import logging
import os
import random
import json
import re

from faker import Faker
from SPARQLWrapper import JSON, SPARQLWrapper
from flask import make_response, render_template

import gender_guesser.detector as gender

from .jeans import real_jeans

from app import app

logger = logging.getLogger(__name__)

DEBUG = True if os.getenv("DEBUG") == "1" else False
URL_BASE = os.getenv("URL_BASE")

SPARQL = SPARQLWrapper("http://fr.dbpedia.org/sparql")

app.debug = DEBUG
app.config["SECRET_KEY"] = os.getenv("SECRET_KEY")

calendrier = {
    1: "janvier",
    2: "février",
    3: "mars",
    4: "avril",
    5: "mai",
    6: "juin",
    7: "juillet",
    8: "août",
    9: "septembre",
    10: "octobre",
    11: "novembre",
    12: "décembre",
}

valid_country = [
    "France",
    "Italie",
    "Autriche",
    "Allemagne",
    "Espagne",
    "Portugal",
    "Luxembourg",
]

valid_nationnality = [
    "français",
    "italien",
    "autrichien",
    "allemand",
    "espagnol",
    "portugais",
    "luxembourgeois",
]

d = gender.Detector()

fake = Faker("fr_FR")

@app.route("/")
def home():
    nom = fake.last_name()

    ddn_jour = str(random.randrange(1, 31)).zfill(2)
    ddn_mois = str(random.randrange(1, 12)).zfill(2)
    ddn_annee = random.randrange(1850, 1950)

    ddc_jour = str(random.randrange(1, 31)).zfill(2)
    ddc_mois = str(random.randrange(1, 12)).zfill(2)
    ddc_annee = ddn_annee + random.randrange(25, 72)

    with open("./app/laposte_hexasmal.json", encoding="UTF8") as file:
        data = json.load(file)

    numero = random.randrange(1, 1000)
    rue = fake.street_name()
    commune_de_vie = data[random.randrange(1, len(data))]["fields"]
    code_postal = commune_de_vie["code_postal"]
    nom_de_la_commune = commune_de_vie["nom_de_la_commune"].title()

    commune_de_naissance = data[random.randrange(1, len(data))]["fields"][
        "nom_de_la_commune"
    ].title()
    commune_de_mort = data[random.randrange(1, len(data))]["fields"][
        "nom_de_la_commune"
    ].title()

    jean_id = str(random.randrange(100000000000, 999999999999))

    top_cni_line = "IDFRA" + nom.upper().replace(" ", "")
    top_cni_line = top_cni_line.ljust(60, "<")
    bottom_cni_line = jean_id + "JEAN"
    bottom_cni_line = bottom_cni_line.ljust(51, "<")
    bottom_cni_line = (
        bottom_cni_line
        + str(ddn_annee)[:2]
        + str(ddn_mois)
        + str(ddn_jour)
        + "0M"
        + str(random.randrange(1, 9))
    )

    font = random.choice(
        ["Rock Salt", "Homemade Apple", "Comforter Brush", "Permanent Marker"]
    )

    jean = {
        "id": jean_id,
        "nom": nom,
        "prenom": "Jean",
        "ddn_jour": ddn_jour,
        "ddn_mois": ddn_mois,
        "ddn_annee": ddn_annee,
        "commune_de_naissance": commune_de_naissance.replace(" ", "-"),
        "ddc_jour": ddc_jour,
        "ddc_mois": ddc_mois,
        "ddc_annee": ddc_annee,
        "commune_de_mort": commune_de_mort.replace(" ", "-"),
        "adresse": f"{numero} {rue} {code_postal} {nom_de_la_commune}",
        "top_cni_line": top_cni_line,
        "bottom_cni_line": bottom_cni_line,
        "font": font,
        "taille": str(round(random.uniform(1.45, 2.10), 2)).replace(".", ","),
    }

    if random.uniform(0, 1) <= 1 / 8:
        funny_names = [
            "Naimarre",
            "Neymare",
            "Cule",
            "Registre",
            "Merde",
            "Racine",
            "Bon Beurre",
            "Bon",
            "Terre",
            "Lève",
            "Nuit",
        ]
        jean["nom"] = random.choice(funny_names)
        jean["is_funny_jean"] = True

    def get_random_person():
        query = (
            """SELECT DISTINCT ?obj
        WHERE {
            ?obj rdf:type foaf:Person . ?obj rdf:type dbo:Person
            FILTER EXISTS {
                ?obj dbo:abstract ?x
                }
            FILTER EXISTS {
                ?obj dbo:citizenship ?y
                }
            ?obj dbo:citizenship dbpedia-fr:"""
            + random.choice(valid_country)
            + """
            } """
        )
        SPARQL.setQuery(query)
        SPARQL.setReturnFormat(JSON)
        persons = SPARQL.query().convert()

        random_id = random.randint(0, 1000)
        my_url = persons["results"]["bindings"][random_id]["obj"]["value"]

        # logger.error(my_url)

        query = f"""
        SELECT *
        WHERE {{ <{my_url}> prop-fr:nom | rdfs:label ?o . }}
        """
        SPARQL.setQuery(query)
        result = SPARQL.query().convert()
        identity = result["results"]["bindings"][0]["o"]["value"]

        query = f"""
        SELECT *
        WHERE {{ <{my_url}> dbo:abstract ?o . }}
        """
        SPARQL.setQuery(query)
        result = SPARQL.query().convert()
        abstract = result["results"]["bindings"][0]["o"]["value"]

        # logger.error(identity)
        # logger.error(abstract)

        return abstract, identity

    def transform_into_jean(abstract, identity):
        bigger_dates = re.findall(
            r"(?:1er|\d+).(?:janvier|fevrier|février|mars|avril|mai|juin|juillet|aout|août|septembre|octobre|novembre|decembre|décembre).\d{4}",
            abstract,
        )

        smaller_dates = re.findall(
            r"(?:janvier|fevrier|février|mars|avril|mai|juin|juillet|aout|août|septembre|octobre|novembre|decembre|décembre).\d{4}",
            abstract,
        )

        years = re.findall(r"\d{4}", abstract)

        # logger.error("1")
        # logger.error(abstract)

        # logger.error(identity)

        gender = d.get_gender(identity.split(" ")[0])
        # logger.error(gender)
        genderletter = "M"
        if "female" in gender:
            genderletter = "F"

        identity = re.sub("\(.*\)", "", identity).split(" ")
        abstract = re.sub(
            f"{identity[0]} ?\w? ?{identity[len(identity)-1]}",
            jean["prenom"] + " " + jean["nom"],
            abstract,
        )

        new_nat = random.choice(valid_nationnality)
        for nat in valid_nationnality:
            abstract = abstract.replace(nat, new_nat)

        # logger.error("2")
        # logger.error(abstract)

        if len(bigger_dates) >= 1:
            abstract = abstract.replace(
                bigger_dates[0],
                str(jean["ddn_jour"])
                + " "
                + calendrier[int(jean["ddn_mois"])]
                + " "
                + str(jean["ddn_annee"]),
            )
            if len(bigger_dates) >= 2:
                abstract = abstract.replace(
                    bigger_dates[1],
                    str(jean["ddc_jour"])
                    + " "
                    + calendrier[int(jean["ddc_mois"])]
                    + " "
                    + str(jean["ddc_annee"]),
                )
        else:
            if len(smaller_dates) >= 1:
                abstract = abstract.replace(
                    smaller_dates[0],
                    calendrier[int(jean["ddn_mois"])] + " " + str(jean["ddn_annee"]),
                )
                if len(smaller_dates) >= 2:
                    abstract = abstract.replace(
                        smaller_dates[1],
                        calendrier[int(jean["ddc_mois"])]
                        + " "
                        + str(jean["ddc_annee"]),
                    )
            else:
                if len(years) >= 1:
                    abstract = abstract.replace(years[0], str(jean["ddn_annee"]))
                    if len(years) >= 2:
                        abstract = abstract.replace(years[1], str(jean["ddc_annee"]))

        # logger.error("3")
        # logger.error(abstract)

        birth_place = re.findall(
            r"née?(?: (?:le|en) \d* ?\w* ?\d*)? (?:à|aux) (?:\w|-|\'|)*(?: \((?:\w|-)*\))?",
            abstract,
        )
        if len(birth_place) == 1:
            city_co_change = birth_place[0].split("à")[1].strip()
            abstract = abstract.replace(city_co_change, jean["commune_de_naissance"])
            death_place = re.findall(
                r"morte?(?: (?:le|en) \d* ?\w* ?\d*)? (?:à|aux) (?:\w|-|\'|)*(?: \((?:\w|-)*\))?",
                abstract,
            )
            if len(death_place) == 1:
                city_co_change = death_place[0].split("à")[1].strip()
                abstract = abstract.replace(city_co_change, jean["commune_de_mort"])
        else:
            just_date_and_place = re.findall(
                r"(?:\d* ?\w* ?\d*)? (?:à|aux) (?:\w|-|\'|)*(?: \((?:\w|-)*\))?",
                abstract,
            )
            if len(just_date_and_place) >= 1:
                city_co_change = just_date_and_place[0].split("à")[1].strip()
                abstract = abstract.replace(
                    city_co_change, jean["commune_de_naissance"]
                )
                if len(just_date_and_place) >= 2:
                    city_co_change = just_date_and_place[1].split("à")[1].strip()
                    abstract = abstract.replace(city_co_change, jean["commune_de_mort"])

        # logger.error("4")
        # logger.error(abstract)

        abstract = re.sub("\(.* : [^[:ascii:]*]*\)", "", abstract)
        abstract = re.sub("\\[[^[:ascii:]*.*-*]*\]", "", abstract)

        abstract = abstract.replace("  ", " ").replace(" ,", ",")

        # logger.error("5")
        # logger.error(abstract)

        return abstract, genderletter

    abstract = ""

    if random.uniform(0, 1) >= 1/16:
        while "Jean" not in abstract:
            try:
                abstract, identity = get_random_person()
                abstract, genderletter = transform_into_jean(abstract, identity)
            except:
                pass
    else:
        jean = random.choice(real_jeans)

        jean_id = str(random.randrange(100000000000, 999999999999))
        jean["id"] = jean_id

        top_cni_line = "IDFRA" + jean["nom"].upper().replace(" ", "")
        jean["top_cni_line"] = top_cni_line.ljust(60, "<")
        bottom_cni_line = jean_id + "JEAN"
        bottom_cni_line = bottom_cni_line.ljust(51, "<")
        jean["bottom_cni_line"] = (
            bottom_cni_line
            + jean["ddn_annee"][:2]
            + jean["ddn_mois"]
            + jean["ddn_jour"]
            + "0M"
            + str(random.randrange(1, 9))
        )
        jean["font"] = "Homemade Apple"

        query = f"""
        SELECT *
        WHERE {{ <{jean["uri"]}> dbo:abstract ?o . }}
        """
        SPARQL.setQuery(query)
        SPARQL.setReturnFormat(JSON)

        result = SPARQL.query().convert()
        abstract = result["results"]["bindings"][0]["o"]["value"]

        genderletter = "M"

    return render_template(
        "home.html",
        page="home",
        abstract=abstract,
        jean=jean,
        gender=genderletter,
        randnumber=str(random.randrange(1, 499)),
    )


@app.route("/faq")
def faq():
    return render_template("faq.html", page="faq")


@app.route("/legal")
def legal():
    return render_template("legal.html", page="legal")


@app.route("/changelog")
def changelog():
    return render_template("changelog.html", page="changelog")


@app.route("/sitemap.xml", methods=["GET"])
def sitemap():
    response = make_response(open("./app/sitemap.xml", encoding="utf-8").read())
    response.headers["Content-type"] = "text/plain"
    return response


@app.route("/robots.txt", methods=["GET"])
@app.route("/robot.txt", methods=["GET"])
def robot():
    response = make_response(open("./app/robots.txt", encoding="utf-8").read())
    response.headers["Content-type"] = "text/plain"
    return response


@app.errorhandler(404)
def page_not_found(e):
    return render_template("404.html"), 404


@app.errorhandler(500)
def server_error(e):
    return render_template("500.html"), 500


@app.errorhandler(403)
def not_authorized(e):
    return render_template("404.html"), 403
